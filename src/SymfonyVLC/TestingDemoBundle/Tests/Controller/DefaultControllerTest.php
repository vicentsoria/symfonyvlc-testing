<?php

namespace SymfonyVLC\TestingDemoBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
	    $this->assertEquals(1, 1);
    }
}
